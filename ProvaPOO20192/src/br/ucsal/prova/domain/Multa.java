package br.ucsal.prova.domain;

public class Multa {
	private String local;
	private String descricao;
	private CategoriaEnum categoria;
	
	public Multa(String local, String descricao, String  categoria) {
		super();
		this.local = local;
		this.descricao = descricao;
		this.categoria = CategoriaEnum.valorPontos(categoria);
	}
	
	
}
