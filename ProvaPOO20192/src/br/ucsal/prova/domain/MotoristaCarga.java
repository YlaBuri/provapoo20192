package br.ucsal.prova.domain;

public class MotoristaCarga extends Motorista{
	private Integer anoExame;

	public MotoristaCarga(Integer numeroCarteira, String nome, Integer anoExame) {
		super(numeroCarteira, nome);
		this.anoExame = anoExame;
	}

	public Integer getAnoExame() {
		return anoExame;
	}

	public void setAnoExame(Integer anoExame) {
		this.anoExame = anoExame;
	}
	
	
}
