package br.ucsal.prova.domain;

import java.util.ArrayList;
import java.util.List;

public class Motorista {
	private Integer numeroCarteira;
	private String nome;
	private List<Multa> multas= new ArrayList<Multa>();
	
	public Motorista(Integer numeroCarteira, String nome) {
		super();
		this.numeroCarteira = numeroCarteira;
		this.nome = nome;
	}
	
	public void setMulta(Multa multa) {
		multas.add(multa);
	}
	
	public Integer getNumeroCarteira() {
		return numeroCarteira;
	}

	public void setNumeroCarteira(Integer numeroCarteira) {
		this.numeroCarteira = numeroCarteira;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Multa> getMultas() {
		return multas;
	}

	public void setMultas(List<Multa> multas) {
		this.multas = multas;
	}
	
	
}
