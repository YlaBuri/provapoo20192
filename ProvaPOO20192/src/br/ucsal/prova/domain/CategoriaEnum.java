package br.ucsal.prova.domain;

public enum CategoriaEnum {
	LEVE(3,"leve"),
	MEDIA(4, "m�dia"), 
	GRAVE(5, "grave"),
	GRAVISSIMA(7, "gravissima");
	
	private Integer pontos;
	private String categoria;
	
	private CategoriaEnum(Integer pontos, String categoria) {
		this.pontos = pontos;
		this.categoria = categoria;
	}

	public Integer getPontos() {
		return pontos;
	}

	public String getCategoria() {
		return categoria;
	}
	
	public static CategoriaEnum valorPontos(String categoria) {
		for (CategoriaEnum c : values()) {
			if(c.getCategoria().equals(categoria)) {
				return c;
			}
		}
		throw new IllegalArgumentException("O categoria " + categoria + " n�o existe na enum");
	}
}
