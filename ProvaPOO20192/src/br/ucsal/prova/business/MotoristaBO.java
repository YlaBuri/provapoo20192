package br.ucsal.prova.business;

import br.ucsal.prova.domain.Motorista;
import br.ucsal.prova.domain.MotoristaCarga;
import br.ucsal.prova.domain.Multa;
import br.ucsal.prova.persistence.MotoristaDAO;

public class MotoristaBO {

	public static void cadastrarMotorista(Integer numeroCarteira, String nome) {
		Motorista motorista = new Motorista(numeroCarteira, nome);
		MotoristaDAO.salvar(motorista);
	}

	public static void cadasrarMotoristaCarga(Integer numeroCarteira, String nome, Integer anoExame) {
		MotoristaCarga motorista = new MotoristaCarga(numeroCarteira, nome, anoExame);
		MotoristaDAO.salvar(motorista);
	}

	public static void adicionarMulta(Integer numeroCarteira, String local, String descricao, String categoria) {
		Multa multa = new Multa(local, descricao, categoria);
		Motorista multado = MotoristaDAO.pesquisar(numeroCarteira);
		multado.setMulta(multa);
	}
}
