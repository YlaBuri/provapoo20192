package br.ucsal.prova.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.prova.domain.Motorista;

public class MotoristaDAO {
	private static List<Motorista> motoristas = new ArrayList<Motorista>();
	
	public static void salvar(Motorista motorista) {
		motoristas.add(motorista);
	}
	
	public static Motorista pesquisar(Integer numero) {
		Motorista busca = null;
		for (Motorista motorista : motoristas) {
			if(motorista.getNumeroCarteira()==numero) {
				busca=motorista;
			}
		}
		
		return busca;
	}
}
